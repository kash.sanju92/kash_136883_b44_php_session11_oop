<?php

// how to create class

class Person{



    public $name;
    //  public $name = 'kalam' likhe dewa jay but ai khetre same thakbe value
    protected $dob;

    private $id;
    //man set korar jonno method nibo akhn

    public function __construct()
    {

        echo "I am inside construct method <br>";
    }


    public function __destruct()
    {
       echo "Pls forgive me  :(" . "<br>";
    }


    public function setName($localName)
    {
        $this->name = $localName; // class er vitore acces korte hobe this diye
    }

    public function setDob($localDob)
    {
        $this->dob = $localDob;
    }

    public function setId($localId)
    {
        $this->id = $localId;
    }

}

class Student extends Person{ // ekhane child holo student parent holo person... class inherit holo

    public function doSomething(){

        echo $this->name . "<br>";
    }


}

$objPerson = new Person();  // ekhane class er baire object ke call kora holo. ekhetre same e thakbe but instance alada

$objPerson-> setName("Jorina from parent class"); // ekhane person tar khetre name set holo

$objPerson-> setId("14");

$objBekti = new Person(); // ekhane same structure a arekti object banalam jar vitore material same

$objBekti-> setName("moyna from parent class");

echo $objBekti->name . "<br>"; // ekhane person er name dekhabe

unset($objBekti);// destroy kore dekhachi destructor method call hoche kina

echo $objPerson->name . "<br>"; // ekhane person er name dekhabe

unset($objPerson);

//echo $objBekti->dob . "<br>"; // ekhane bekti er name dekhabe. jehetu protected tai acceess hobe na

//echo $objPerson->id . "<br>"; eta access korte debe na...

$objNewStudent = new Student(); // child class theke banachi object

$objNewStudent-> setName("rahim from child class");

echo $objNewStudent-> doSomething();

unset($objNewStudent);

